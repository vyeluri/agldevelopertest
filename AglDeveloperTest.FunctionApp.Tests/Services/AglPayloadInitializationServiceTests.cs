﻿using AglDeveloperTest.FunctionApp.Services;
using AglDeveloperTest.FunctionApp.Services.Properties;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AglDeveloperTest.FunctionApp.Tests.Services
{
    /// <summary>
    /// Represents an entity of <see cref="AglPayloadInitializationService"/> class
    /// </summary>
    [TestFixture]
    public class AglPayloadInitializationServiceTests
    {
        /// <summary>
        /// Test method to return throw exception if payload properties are null
        /// </summary>
        [Test]
        public void InvokeAsync_WithNullPayloadPropertites_ShouldThrowException()
        {
            var initializePayloadService = new AglPayloadInitializationService();
            
            Assert.Throws<NullReferenceException>(() => initializePayloadService
                                                        .InvokeAsync(new AglPayloadInitializationProperty())
                                                        .ConfigureAwait(false));
        }

        /// <summary>
        /// Tests whether initialized payload with data should return result or not.
        /// </summary>
        /// <returns></returns>
        [Test]
        public async Task InvokeAsync_InitializedPayload_ShouldReturnResultAsync()
        {
            var initializePayloadService = new AglPayloadInitializationService();

            var initializePayloadProperty = new AglPayloadInitializationProperty()
            {
                People = new List<Models.Person>(),
                PetType = Models.PetType.Cat
            };

            await initializePayloadService.InvokeAsync(initializePayloadProperty).ConfigureAwait(false);

            Assert.That(initializePayloadProperty.IsInvoked, Is.True);
        }
    }
}
