﻿using AglDeveloperTest.FunctionApp.Config;
using AglDeveloperTest.FunctionApp.Services;
using AglDeveloperTest.FunctionApp.Services.Properties;
using AglDeveloperTest.FunctionApp.Tests.Services.Helper;
using Moq;
using NUnit.Framework;
using System;
using System.Net.Http;

namespace AglDeveloperTest.FunctionApp.Tests.Services
{
    /// <summary>
    /// Represents an entity of <see cref="AglPayloadLoadService"/> class
    /// </summary>
    [TestFixture]
    public class AglPayloadLoadServiceTests
    {
        /// <summary>
        /// Test method to throw exception class initialized with null parameters
        /// </summary>
        /// <param name="appConfig"><see cref="AppConfig"/> instance</param>
        /// <param name="httpClient"><see cref="HttpClient"/> instance</param>
        [Test]
        [TestCase(null, null)]
        [TestCaseSource(typeof(AglPayLoadSerivceHelperTest), "AppConfigValue")]
        public void Constructor_WithNullParameters_ShouldThrowException(AppConfig appConfig, HttpClient httpClient)
        {
            Assert.Throws<ArgumentNullException>(() => new AglPayloadLoadService(appConfig, httpClient));
        }

        /// <summary>
        /// Test method to throw exception or not when payload is null
        /// </summary>
        /// <param name="payloadLoadProperty"></param>
        [Test]
        [TestCaseSource(typeof(AglPayLoadSerivceHelperTest), "MockPayLoadPropertyValue")]
        public void InvokeAsync_WithNullPayload_ShouldThrowException(AglPayloadLoadProperty payloadLoadProperty)
        {
            var appConfig = new Mock<AppConfig>();

            Assert.Throws<ArgumentNullException>(() => new AglPayloadLoadService(appConfig.Object, null)
                                                            .InvokeAsync(payloadLoadProperty));
        }
    }
}
