﻿using AglDeveloperTest.FunctionApp.Config;
using AglDeveloperTest.FunctionApp.Services.Properties;
using System.Collections;
using System.Collections.Generic;

namespace AglDeveloperTest.FunctionApp.Tests.Services.Helper
{
    /// <summary>
    /// Represents an helper entity of <see cref="AglPayLoadSerivceHelperTest"/> to create <see cref="IEnumerable"/> return type methods
    /// </summary>
    public class AglPayLoadSerivceHelperTest
    {
        /// <summary>
        /// Gets a static <see cref="IEnumerable"/> array of objects
        /// </summary>
        public static IEnumerable<object[]> AppConfigValue
        {
            get
            {
                yield return new object[]
                {
                    new AppConfig(),
                    null
                };
            }
        }

        /// <summary>
        /// Gets a static <see cref="IEnumerable"/> values
        /// </summary>
        public static IEnumerable MockPayLoadPropertyValue
        {
            get
            {
                yield return new AglPayloadLoadProperty();
            }
        }

    }
}
