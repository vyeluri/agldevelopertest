﻿using AglDeveloperTest.FunctionApp.Factory;
using AglDeveloperTest.FunctionApp.Functions.Interfaces;
using AglDeveloperTest.FunctionApp.Tests.Helpers;
using NUnit.Framework;

namespace AglDeveloperTest.FunctionApp.Tests.Factory
{
    /// <summary>
    /// This represents the test entity for the <see cref="AglFunctionFactory"/> class.
    /// </summary>
    [TestFixture]
    public class AglFunctionFactoryTests
    {
        /// <summary>
        /// Tests whether factory instance should return result or not.
        /// </summary>
        [Test]
        public void Create_FactoryInstance_ShouldNotBeNull()
        {
            //Arrange
            var log = new MockTraceWriter(System.Diagnostics.TraceLevel.Info);
            var factory = new AglFunctionFactory();

            //Act
            IAglHttpTriggerFunction function = factory.Create(log);

            //Assert
            Assert.That(function, Is.Not.Null);
        }
    }
}
