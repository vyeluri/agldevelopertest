﻿using AglDeveloperTest.FunctionApp.Extensions;
using NUnit.Framework;
using System;

namespace AglDeveloperTest.FunctionApp.Tests.Extensions
{
    /// <summary>
    /// Represents the test entity for the <see cref="GenericExtension"/> class.
    /// </summary>
    [TestFixture]
    public class GenericExtensionTests
    {
        /// <summary>
        /// Tests method whether object should return <c>True</c> or not given reference is null.
        /// </summary>
        [Test]
        public void IsNullOrDefault_ObjectReferenceNull_ShouldReturnTrue()
        {
            var result = GenericExtension.IsNullOrDefault((object)null);
            Assert.That(result, Is.True);

            result = GenericExtension.IsNullOrDefault(default(object));
            Assert.That(result, Is.True);
        }

        /// <summary>
        /// Tests the method whether to throw en exception or not.
        /// </summary>
        [Test]
        public void ThrowIfNullOrDefault_ObjectReferenceNull_ShouldThrowException()
        {
            Assert.Throws<ArgumentNullException>(() => GenericExtension.ThrowIfNullOrDefault((object)null));

            Assert.Throws<ArgumentNullException>(() => GenericExtension.ThrowIfNullOrDefault(default(object)));
        }
    }
}
