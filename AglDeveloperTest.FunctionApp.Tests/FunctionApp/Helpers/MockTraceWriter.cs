﻿using Microsoft.Azure.WebJobs.Host;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace AglDeveloperTest.FunctionApp.Tests.Helpers
{
    /// <summary>
    /// Represents a test entity for Mock <see cref="TraceWriter"/> class
    /// </summary>
    public class MockTraceWriter : TraceWriter
    {
        private TraceLevel _level;
        private readonly ConcurrentBag<TraceEvent> _traces;

        /// <summary>
        /// Initializes a new instance of <see cref="MockTraceWriter"/>
        /// </summary>
        /// <param name="level"></param>
        public MockTraceWriter(TraceLevel level) : base(level)
        {
            _level = level;
            _traces = new ConcurrentBag<TraceEvent>();
        }

        /// <summary>
        /// Overrides Trace method to create custom trace event
        /// </summary>
        /// <param name="traceEvent"><see cref="TraceEvent"/> instance</param>
        public override void Trace(TraceEvent traceEvent)
        {
            _traces.Add(traceEvent);
        }

        /// <summary>
        /// Get or Sets collection of <see cref="TraceEvent"/>
        /// </summary>
        public List<TraceEvent> Traces => _traces.ToList();
    }
}
