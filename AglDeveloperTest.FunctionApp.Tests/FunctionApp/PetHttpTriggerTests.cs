using System.Net.Http;
using System.Threading.Tasks;
using AglDeveloperTest.FunctionApp.Factory;
using AglDeveloperTest.FunctionApp.Functions;
using AglDeveloperTest.FunctionApp.Functions.Interfaces;
using AglDeveloperTest.FunctionApp.Tests.Helpers;
using Microsoft.Azure.WebJobs.Host;
using Moq;
using NUnit.Framework;

namespace AglDeveloperTest.FunctionApp.Tests.FunctionApp
{
    /// <summary>
    /// Represents the test class reference for <see cref="PetHttpTrigger"/>.
    /// </summary>
    [TestFixture]
    public class PetHttpTriggerTests
    {
        /// <summary>
        /// Tests whether the method should return result or not.
        /// </summary>
        [Test]
        public async Task Run_WhenInitilizeWithMockFactory_ShouldReturnResultAsync()
        {
            //Arrange
            var log = new MockTraceWriter(System.Diagnostics.TraceLevel.Info);
            var req = new HttpRequestMessage();
            var res = new HttpResponseMessage();

            var httpTriggerFunction = new Mock<IAglHttpTriggerFunction>();
            httpTriggerFunction.Setup(l => l.Log).Returns(log);
            httpTriggerFunction.Setup(p => p.InvokeAsync(It.IsAny<HttpRequestMessage>(), It.IsAny<AglFunctionOption>()))
                .ReturnsAsync(res);
            var factory = new Mock<IAglFunctionFactory>();
            factory.Setup(p => p.Create(It.IsAny<TraceWriter>())).Returns(httpTriggerFunction.Object);
            
            PetHttpTrigger.AglFunctionFactory = factory.Object;
            
            //Act
            var result = await PetHttpTrigger.Run(req, log).ConfigureAwait(false);

            //Assert
            Assert.That(result, Is.Not.Null);
        }
    }
}
