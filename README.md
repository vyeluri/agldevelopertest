# AGL Developer Test

This project consumes AGL [Web Service](http://agl-developer-test.azurewebsites.net/people.json) using Azure Serverless Functions and returns a html page.

# Project Requirements

Consume Json payload from AGL [Web Service](http://agl-developer-test.azurewebsites.net/people.json) and output a list of all the cats in alphabetical order under a heading of the gender of their owner as below.

**Male**

- Garfield
- Jim
- Max
- Tom

**Female**

- Garfield
- Simba
- Tabby

# Development Software

- Visual Studio 2017
- Azure Functions Cli Tool

Required Local environment variables and pre-set **PetType as Cat** in AglServiceConfig under Config folder. 

Create **local.settings.json** file in _AglDeveloperTest.FunctionApp_ project with below minimal contents to run the PetHttpTriggerFunction locally.

```json
{
  "IsEncrypted": false,
  "Values": {
    "AglServiceEndpoint": "http://agl-developer-test.azurewebsites.net/people.json"
  }
}
```

# Build and Run the Application
- Clone the repository from [bitbucket.org](https://vyeluri@bitbucket.org/vyeluri/agldevelopertest.git) 
- Open project solution - AglDeveloperTest.sln
- Build and run the solution in Visual Stuido 2017
- Direct to [http://localhost:7071/pet](http://localhost:7071/pet) in your web browser to view the output specified in the requirements.

# Testing 
Test cases has been written using NUnit and Moq framework.

- Test project has been named as AglDeveloperTest.FunctionApp.Tests
- Open the Test Explorer in Visual Studio 2017 to view all tests
- Click _RunAll_ in Test Explorer to see the result.

# Output 

**Male**

- Garfield
- Jim
- Max
- Tom

**Female**

- Garfield
- Simba
- Tabby

# Author
Venkatesh Yeluri

LinkedIn : [https://www.linkedin.com/in/vyeluri5/](https://www.linkedin.com/in/vyeluri5/) 