﻿using AglDeveloperTest.FunctionApp.Functions.Interfaces;
using Microsoft.Azure.WebJobs.Host;
using System;

namespace AglDeveloperTest.FunctionApp.Factory
{
    /// <summary>
    /// Provides interface to <see cref="AglFunctionFactory"/> class implements <see cref="IDisposable"/>
    /// </summary>
    public interface IAglFunctionFactory : IDisposable
    {
        /// <summary>
        /// Initializes a new instance of <see cref="AglFunctionFactory.Create"/> class 
        /// </summary>
        /// <param name="traceWriter"><see cref="TraceWriter"/> instance for tracing</param>
        /// <returns>Object of <see cref="AglTestHttpTriggerFunction"/> class</returns>
        IAglHttpTriggerFunction Create(TraceWriter traceWriter);
    }
}
