﻿using System;
using AglDeveloperTest.FunctionApp.DependencyResolution;
using AglDeveloperTest.FunctionApp.Functions.Interfaces;
using Microsoft.Azure.WebJobs.Host;
using Ninject;

namespace AglDeveloperTest.FunctionApp.Factory
{
    /// <summary>
    /// Class <see cref="AglFunctionFactory"/> implements <see cref="IAglFunctionFactory"/> factory methods
    /// </summary>
    public class AglFunctionFactory : IAglFunctionFactory
    {
        private readonly IKernel kernel;
        private bool isDisposed = false;

        /// <summary>
        /// Initializes a new instance of the <see cref="AglFunctionFactory"/> class.
        /// </summary>
        public AglFunctionFactory()
        {
            kernel = new StandardKernel(new IoCModule());
        }

        /// <summary>
        /// Initializes a new instance of <see cref="AglFunctionFactory.Create(TraceWriter)"/> class 
        /// </summary>
        /// <param name="traceWriter"><see cref="TraceWriter"/> instance for tracing</param>
        /// <returns>Object of <see cref="AglTestHttpTriggerFunction"/> class</returns>
        public IAglHttpTriggerFunction Create(TraceWriter traceWriter)
        {
            var function = kernel.Get<IAglHttpTriggerFunction>();
            function.Log = traceWriter;
            return function;
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting    
        /// unmanaged resources
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting    
        /// unmanaged resources
        /// </summary>
        /// <param name="dispose"></param>
        protected void Dispose(bool dispose)
        {
            if (!isDisposed)
            {
                if (dispose)
                {
                    //Clean up managed objects
                }

                //To clean up unmanaged objects
                isDisposed = true;
            }
        }

        /// <summary>
        /// Destructor to release unmanaged resouces
        /// </summary>
        ~AglFunctionFactory()
        {
            Dispose(false);
        }
    }
}
