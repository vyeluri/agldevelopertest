﻿using Microsoft.Azure.WebJobs.Host;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace AglDeveloperTest.FunctionApp.Functions.Interfaces
{
    /// <summary>
    /// Provides interfaces to the <see cref="IAglHttpTriggerFunction"/> class and implements <see cref="IDisposable"/>.
    /// </summary>
    public interface IAglHttpTriggerFunction : IDisposable
    {
        /// <summary>
        /// Gets or sets the <see cref="TraceWriter"/> instance.
        /// </summary>
        TraceWriter Log { get; set; }

        /// <summary>
        /// Invokes Task of <see cref="HttpResponseMessage"/>.
        /// </summary>
        /// <param name="req"><see cref="HttpRequestMessage"/> instance</param>
        /// <param name="options"><see cref="AglFunctionOption"/> instance</param>
        /// <returns></returns>
        Task<HttpResponseMessage> InvokeAsync(HttpRequestMessage req, AglFunctionOption options);
    }
}
