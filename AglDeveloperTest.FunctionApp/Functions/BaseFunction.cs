﻿
namespace AglDeveloperTest.FunctionApp.Functions
{
    /// <summary>
    /// Represents the base options for function.
    /// </summary>
    public class BaseFunction
    {
        /// <summary>
        /// Gets or sets a value for sevice invocation status
        /// </summary>
        public bool IsInvoked { get; set; }
    }
}
