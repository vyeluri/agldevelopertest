﻿using AglDeveloperTest.FunctionApp.Extensions;
using AglDeveloperTest.FunctionApp.Functions.Interfaces;
using AglDeveloperTest.FunctionApp.Services.Interfaces;
using AglDeveloperTest.FunctionApp.Services.Properties;
using Microsoft.Azure.WebJobs.Host;
using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace AglDeveloperTest.FunctionApp.Functions
{
    /// <summary>
    /// Represents the function to process for AGL web service
    /// </summary>
    public class AglHttpTriggerFunction : IAglHttpTriggerFunction
    {
        private bool isDisposed = false;
        private readonly IAglPayloadLoadService _loadService;
        private readonly IAglPayloadInitializationService _initializeProcessService;

        private AglPayloadLoadProperty _payloadProperty;
        private AglPayloadInitializationProperty _payloadInitializationProperty;

        /// <summary>
        /// Initializes a new instance of the <see cref="AglHttpTriggerFunction"/> class.
        /// </summary>
        /// <param name="loadService"><see cref="IAglPayloadLoadService"/> instance.</param>
        /// <param name="initializeProcessService"><see cref="IAglPayloadInitializationService"/> instance.</param>
        public AglHttpTriggerFunction(IAglPayloadLoadService loadService, IAglPayloadInitializationService initializeProcessService)
        {
            this._loadService = loadService.ThrowIfNullOrDefault();
            this._initializeProcessService = initializeProcessService.ThrowIfNullOrDefault();
        }

        /// <summary>
        /// Invokes Task of <see cref="HttpResponseMessage"/>.
        /// </summary>
        /// <param name="req"><see cref="HttpRequestMessage"/> instance</param>
        /// <param name="options"><see cref="AglFunctionOption"/> instance</param>
        /// <returns></returns>
        public async Task<HttpResponseMessage> InvokeAsync(HttpRequestMessage req, AglFunctionOption options)
        {
            req.ThrowIfNullOrDefault();

            options.ThrowIfNullOrDefault();
            
            this._payloadProperty = new AglPayloadLoadProperty();

            await this._loadService.InvokeAsync(this._payloadProperty).ConfigureAwait(false);

            if (!this._payloadProperty.IsInvoked)
            {
                return req.CreateErrorResponse(HttpStatusCode.InternalServerError, "Error loading AGL payload information");
            }

            this._payloadInitializationProperty = new AglPayloadInitializationProperty()
            {
                People = this._payloadProperty.People,
                PetType = options.PetType
            };

            await this._initializeProcessService.InvokeAsync(this._payloadInitializationProperty).ConfigureAwait(false);

            if (!this._payloadInitializationProperty.IsInvoked)
            {
                return req.CreateErrorResponse(HttpStatusCode.InternalServerError, "Error initializing AGL payload information");
            }
            
            var html = new StringBuilder();
            html.AppendLine("<html><body>");
            html.AppendLine(string.Join(string.Empty, this._payloadInitializationProperty.Group));
            html.AppendLine("</body></html>");

            var content = new StringContent(html.ToString(), Encoding.UTF8, "text/html");
            var res = new HttpResponseMessage(HttpStatusCode.OK) { Content = content };

            return res;
        }

        /// <summary>
        /// Gets or sets the <see cref="TraceWriter"/> instance.
        /// </summary>
        public TraceWriter Log { get; set; }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        /// <param name="dispose">Value to indicate whether to dispose managed resources or not.</param>
        protected void Dispose(bool dispose)
        {
            if (!isDisposed)
            {
                if (dispose)
                {
                    //Clean up managed objects
                }

                //To clean up unmanaged objects
                isDisposed = true;
            }
        }

        /// <summary>
        /// Destructor to release unmanaged resouces
        /// </summary>
        ~AglHttpTriggerFunction()
        {
            Dispose(false);
        }
    }
}
