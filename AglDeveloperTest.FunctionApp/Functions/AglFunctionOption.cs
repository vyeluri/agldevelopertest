﻿using AglDeveloperTest.FunctionApp.Models;

namespace AglDeveloperTest.FunctionApp.Functions
{
    /// <summary>
    /// Represents function options for <see cref="AglFunctionOption"/> class.
    /// </summary>
    public class AglFunctionOption : BaseFunction
    {
        /// <summary>
        /// Gets or sets the <see cref="Models.PetType"/> value.
        /// </summary>
        public PetType PetType { get; set; }
    }
}
