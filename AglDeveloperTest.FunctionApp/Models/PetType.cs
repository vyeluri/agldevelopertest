﻿namespace AglDeveloperTest.FunctionApp.Models
{
    /// <summary>
    /// Specifies the PetType.
    /// </summary>
    public enum PetType
    {
        /// <summary>
        /// Identifies as Cat.
        /// </summary>
        Cat = 1,

        /// <summary>
        /// Identifies as Dog.
        /// </summary>
        Dog = 2,

        /// <summary>
        /// Identifies as Fish.
        /// </summary>
        Fish = 3
    }
}
