﻿using System.Collections.Generic;

namespace AglDeveloperTest.FunctionApp.Models
{
    /// <summary>
    /// Represents the model for <see cref="Person"/>.
    /// </summary>
    public class Person
    {
        /// <summary>
        /// Gets or sets the Name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the Gender
        /// </summary>
        public string Gender { get; set; }

        /// <summary>
        /// Gets or sets the Age.
        /// </summary>
        public int Age { get; set; }

        /// <summary>
        /// Gets or sets the list of <see cref="Pet"/> objects.
        /// </summary>
        public List<Pet> Pets { get; set; } = new List<Pet>();
    }
}
