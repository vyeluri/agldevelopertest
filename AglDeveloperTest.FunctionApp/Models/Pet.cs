﻿
namespace AglDeveloperTest.FunctionApp.Models
{
    /// <summary>
    /// Represents the model for <see cref="Pet"/>.
    /// </summary>
    public class Pet
    {
        /// <summary>
        /// Gets or sets the Name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the Type.
        /// </summary>
        public string Type { get; set; }
    }
}
