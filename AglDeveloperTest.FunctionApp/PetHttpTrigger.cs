using System.Net.Http;
using System.Threading.Tasks;
using AglDeveloperTest.FunctionApp.Config;
using AglDeveloperTest.FunctionApp.Factory;
using AglDeveloperTest.FunctionApp.Functions;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Azure.WebJobs.Host;

namespace AglDeveloperTest.FunctionApp
{
    /// <summary>
    /// Represents <see cref="PetHttpTrigger"/> class
    /// </summary>
    public static class PetHttpTrigger
    {
        /// <summary>
        /// Gets or sets the <see cref="AglFunctionFactory"/> instance.
        /// </summary>
        public static IAglFunctionFactory AglFunctionFactory { get; set; } = new AglFunctionFactory();

        /// <summary>
        /// Runs the function.
        /// </summary>
        /// <param name="req"><see cref="HttpRequestMessage"/> instance.</param>
        /// <param name="log"><see cref="TraceWriter"/> instance.</param>
        /// <returns>Returns the <see cref="HttpResponseMessage"/> instance.</returns>
        [FunctionName("PetHttpTrigger")]
        public static async Task<HttpResponseMessage> Run(
            [HttpTrigger(AuthorizationLevel.Function, "get", Route = "pet")]
            HttpRequestMessage req, 
            TraceWriter log)
        {

            log.Info("C# HTTP trigger function processed a request.");

            var result = await AglFunctionFactory.Create(log)
                .InvokeAsync(req, new AglFunctionOption() { PetType = AglServiceConfig.PetType })
                .ConfigureAwait(false);

            return result as HttpResponseMessage;
        }
    }
}
