﻿using AglDeveloperTest.FunctionApp.Functions;
using AglDeveloperTest.FunctionApp.Functions.Interfaces;
using AglDeveloperTest.FunctionApp.Services;
using AglDeveloperTest.FunctionApp.Services.Interfaces;
using Ninject.Modules;

namespace AglDeveloperTest.FunctionApp.DependencyResolution
{
    /// <summary>
    /// Represents a module to bind dependencies
    /// </summary>
    public class IoCModule : NinjectModule
    {
        // <summary>
        /// Overrides <see cref="NinjectModule"/> method <see cref="Load()"/>.
        /// </summary>
        public override void Load()
        {
            Bind<IAglHttpTriggerFunction>().To<AglHttpTriggerFunction>();
            Bind<IAglPayloadLoadService>().To<AglPayloadLoadService>();
            Bind<IAglPayloadInitializationService>().To<AglPayloadInitializationService>();
        }
    }
}
