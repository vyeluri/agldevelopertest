﻿using AglDeveloperTest.FunctionApp.Models;
using System;

namespace AglDeveloperTest.FunctionApp.Config
{
    /// <summary>
    /// Represents <see cref="AglServiceConfig"/> to configure endpoint and Function option type
    /// </summary>
    public class AglServiceConfig
    {
        public const string AglServiceEndpoint = "AglServiceEndpoint";

        /// <summary>
        /// Gets the <see cref="PetType"/> enum
        /// </summary>
        public static PetType PetType { get; } = PetType.Cat;

        /// <summary>
        /// Gets the endpoint URL.
        /// </summary>
        public string Endpoint => Environment.GetEnvironmentVariable(AglServiceEndpoint);
    }
}
