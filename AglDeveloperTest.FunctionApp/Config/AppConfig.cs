﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;
using System.Net.Http.Formatting;

namespace AglDeveloperTest.FunctionApp.Config
{
    public class AppConfig
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AppConfig"/> class.
        /// </summary>
        public AppConfig()
        {
            var serialiserSettings = new JsonSerializerSettings()
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver(),
                Converters = { new StringEnumConverter() },
                Formatting = Formatting.Indented,
                NullValueHandling = NullValueHandling.Ignore,
                MissingMemberHandling = MissingMemberHandling.Ignore
            };
            this.Formatter = new JsonMediaTypeFormatter() { SerializerSettings = serialiserSettings };
            this.SerialiserSettings = serialiserSettings;
        }

        /// <summary>
        /// Gets the <see cref="AglServiceConfig"/> instance.
        /// </summary>
        public virtual AglServiceConfig AglSeriveConfig => new AglServiceConfig();

        /// <summary>
        /// Gets the <see cref="MediaTypeFormatter"/> instance.
        /// </summary>
        public virtual MediaTypeFormatter Formatter { get; }

        /// <summary>
        /// Gets the <see cref="JsonSerializerSettings"/> instance.
        /// </summary>
        public virtual JsonSerializerSettings SerialiserSettings { get; }
    }
}
