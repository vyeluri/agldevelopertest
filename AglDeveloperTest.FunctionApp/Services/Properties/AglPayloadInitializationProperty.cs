﻿using AglDeveloperTest.FunctionApp.Models;
using System.Collections.Generic;

namespace AglDeveloperTest.FunctionApp.Services.Properties
{
    /// <summary>
    /// Represents the properties for <see cref="AglPayloadInitializationProperty"/>.
    /// </summary>
    public class AglPayloadInitializationProperty : BaseProperty
    {
        /// <summary>
        /// Gets or sets the list of <see cref="Person"/> objects.
        /// </summary>
        public List<Person> People { get; set; }

        /// <summary>
        /// Gets or sets the <see cref="Models.PetType"/> value.
        /// </summary>
        public PetType PetType { get; set; }

        /// <summary>
        /// Gets or sets the collection of group of pets.
        /// </summary>
        public List<string> Group { get; set; }
    }
}
