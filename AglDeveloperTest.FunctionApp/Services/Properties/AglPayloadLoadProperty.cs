﻿using AglDeveloperTest.FunctionApp.Models;
using System.Collections.Generic;

namespace AglDeveloperTest.FunctionApp.Services.Properties
{
    /// <summary>
    /// Represents the properties for <see cref="AglPayloadLoadProperty"/>.
    /// </summary>
    public class AglPayloadLoadProperty : BaseProperty
    {
        /// <summary>
        /// Gets or sets the list of <see cref="Person"/> objects.
        /// </summary>
        public List<Person> People { get; set; }
    }
}
