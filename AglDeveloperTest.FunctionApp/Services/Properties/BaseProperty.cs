﻿
namespace AglDeveloperTest.FunctionApp.Services.Properties
{
    /// <summary>
    /// Represents the base options entity for service.
    /// </summary>
    public class BaseProperty
    {
        /// <summary>
        /// Gets or sets a value to service invocation status.
        /// </summary>
        public bool IsInvoked { get; set; }
    }
}
