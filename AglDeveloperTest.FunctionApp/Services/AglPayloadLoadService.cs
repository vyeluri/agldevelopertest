﻿using AglDeveloperTest.FunctionApp.Config;
using AglDeveloperTest.FunctionApp.Extensions;
using AglDeveloperTest.FunctionApp.Models;
using AglDeveloperTest.FunctionApp.Services.Interfaces;
using AglDeveloperTest.FunctionApp.Services.Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Threading.Tasks;
using System.Web;

namespace AglDeveloperTest.FunctionApp.Services
{
    /// <summary>
    /// Represents the service to load payload consumed from AGL web service
    /// </summary>
    public class AglPayloadLoadService : IAglPayloadLoadService
    {
        private readonly AppConfig _appConfig;
        private readonly HttpClient _httpClient;
        private readonly List<MediaTypeFormatter> _formatters;
        private bool isDisposed = false;

        /// <summary>
        /// Initializes a new instance of the <see cref="AglPayloadLoadService"/> class.
        /// </summary>
        /// <param name="appConfig"><see cref="AppConfig"/> instance.</param>
        /// <param name="httpClient"><see cref="HttpClient"/> instance.</param>
        public AglPayloadLoadService(AppConfig appConfig, HttpClient httpClient)
        {
            this._appConfig = appConfig.ThrowIfNullOrDefault();
            this._httpClient = httpClient.ThrowIfNullOrDefault();
            this._formatters = new[] { this._appConfig.Formatter }.ToList();
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        /// <param name="dispose"><see cref="bool"/> instance.</param>
        protected void Dispose(bool dispose)
        {
            if (!isDisposed)
            {
                if (dispose)
                {
                    //Clean up managed objects
                }

                //To clean up unmanaged objects
                isDisposed = true;
            }
        }

        /// <summary>
        /// Invokes the service
        /// </summary>
        /// <param name="payloadProperty"><see cref="AglPayloadLoadProperty"/> instance</param>
        /// <returns></returns>
        public async Task InvokeAsync(AglPayloadLoadProperty payloadLoadProperty)
        {
            payloadLoadProperty.ThrowIfNullOrDefault();

            var serviceUri = _appConfig.AglSeriveConfig.Endpoint;
            using (var response = await this._httpClient.GetAsync(serviceUri).ConfigureAwait(false))
            {
                if (!response.IsSuccessStatusCode)
                {
                    throw new HttpException((int)response.StatusCode, response.ReasonPhrase);
                }

                var people = await response.Content
                                           .ReadAsAsync<List<Person>>(this._formatters)
                                           .ConfigureAwait(false);

                payloadLoadProperty.People = people;
                payloadLoadProperty.IsInvoked = true;
            }
        }
    }
}
