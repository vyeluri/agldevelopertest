﻿using AglDeveloperTest.FunctionApp.Extensions;
using AglDeveloperTest.FunctionApp.Services.Interfaces;
using AglDeveloperTest.FunctionApp.Services.Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AglDeveloperTest.FunctionApp.Services
{
    /// <summary>
    /// Represents the service to initialize the processing of AGL payload.
    /// </summary>
    public class AglPayloadInitializationService : IAglPayloadInitializationService
    {
        private bool isDisposed = false;

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        /// <param name="dispose"><see cref="bool"/> instance</param>
        protected void Dispose(bool dispose)
        {
            if (!isDisposed)
            {
                if (dispose)
                {
                    //Clean up managed objects
                }

                //To clean up unmanaged objects
                isDisposed = true;
            }
        }

        /// <summary>
        /// Invokes the service
        /// </summary>
        /// <param name="initializationProperty"><see cref="AglPayloadInitializationProperty"/> instance.</param>
        /// <returns></returns>
        public Task InvokeAsync(AglPayloadInitializationProperty initializationProperty)
        {
            initializationProperty.ThrowIfNullOrDefault();

            var serviceOptions = initializationProperty;
            serviceOptions.ThrowIfNullOrDefault();

            Dictionary<string, List<string>> peoplePets = new Dictionary<string, List<string>>();

            foreach (var people in serviceOptions.People)
            {
                foreach (var pet in people.Pets)
                {
                    if (pet.Type == serviceOptions.PetType.ToString())
                    {
                        if (peoplePets.ContainsKey(people.Gender))
                        {
                            peoplePets[people.Gender].Add(pet.Name);
                        }
                        else
                        {
                            peoplePets.Add(people.Gender, new List<string>() { pet.Name });
                        }
                    }
                }
            }

            serviceOptions.Group = new List<string>();
            serviceOptions.IsInvoked = true;
            foreach (KeyValuePair<string, List<string>> item in peoplePets)
            {
                serviceOptions.Group.Add($"<h1>{item.Key}</h1><ul>{string.Join(string.Empty, item.Value.OrderBy(val => val).Select(val => $"<li>{val}</li>"))}</ul>");
            }

            return Task.CompletedTask;
        }
    }
}
