﻿using AglDeveloperTest.FunctionApp.Services.Properties;
using System;
using System.Threading.Tasks;

namespace AglDeveloperTest.FunctionApp.Services.Interfaces
{
    /// <summary>
    /// Provides interfaces to the <see cref="AglPayloadLoadService"/> class and implements <see cref="IDisposable"/> interface.
    /// </summary>
    public interface IAglPayloadLoadService : IDisposable
    {
        /// <summary>
        /// Invokes the service
        /// </summary>
        /// <param name="payloadProperty"><see cref="AglPayloadLoadProperty"/> instance</param>
        /// <returns></returns>
        Task InvokeAsync(AglPayloadLoadProperty payloadProperty);
    }
}
