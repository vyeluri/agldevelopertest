﻿using AglDeveloperTest.FunctionApp.Services.Properties;
using System;
using System.Threading.Tasks;

namespace AglDeveloperTest.FunctionApp.Services.Interfaces
{
    /// <summary>
    /// Provides interfaces to the <see cref="AglPayloadInitializationService"/> class and implements <see cref="IDisposable"/>.
    /// </summary>
    public interface IAglPayloadInitializationService : IDisposable
    {
        /// <summary>
        /// Invokes the service
        /// </summary>
        /// <param name="initializationProperty"><see cref="AglPayloadInitializationProperty"/> instance.</param>
        /// <returns></returns>
        Task InvokeAsync(AglPayloadInitializationProperty initializationProperty);
    }
}
